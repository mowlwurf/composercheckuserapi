<?php

namespace DevGarden\UserApiBundle\Service;

/**
 * Class CallService
 * @package DevGarden\ComposerCheckUserApiBundle\Service
 */
class CallService
{
    /**
     * @var
     */
    protected $ch;

    public function __construct($url)
    {
        $this->ch = curl_init();
        $this->setOption(CURLOPT_URL, $url);
        $this->setOption(CURLOPT_RETURNTRANSFER, TRUE);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function send(array $data){
        $data_string = '';
        foreach($data as $key=>$value) { $data_string .= $key.'='.$value.'&'; }
        rtrim($data_string, '&');
        $this->setOption(CURLOPT_POST, count($data));
        $this->setOption(CURLOPT_POSTFIELDS, $data_string);
        return curl_exec($this->ch);
    }

    /**
     * @param string $option
     * @param string $value
     */
    protected function setOption($option, $value){
        curl_setopt($this->ch, $option, $value);
    }
}