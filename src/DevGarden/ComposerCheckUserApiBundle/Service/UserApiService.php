<?php

namespace DevGarden\UserApiBundle\Service;

/**
 * Class UserApiService
 * @package DevGarden\ComposerCheckUserApiBundle\Service
 */
class UserApiService
{
    CONST CHECK_URL = 'http://composercheck.dev-garden.com/';
    CONST TOKEN_URL = 'http://composercheck.dev-garden.com/token';

    /**
     * @var string
     */
    protected $composerJsonFile;

    /**
     * @var string
     */
    protected $composerLockFile;

    /**
     * @var string
     */
    protected $userMail;

    /**
     * @var string
     */
    protected $userPass;

    /**
     * @var string
     */
    protected $userToken;

    /**
     * @var string
     */
    protected $projectName;

    /**
     * @var array
     */
    protected $additionalMailReceivers = [];

    public function __construct($mail=false, $pass=false)
    {
        if ($mail) {
            $this->setUserMail($mail);
        }
        if ($pass) {
            $this->setUserPass($pass);
        }
    }

    /**
     * @param string $mail
     */
    public function addMailReceiver($mail){
        array_push($this->additionalMailReceivers, $mail);
    }

    /**
     * @param array $mails
     */
    public function addMailReceivers($mails){
        foreach ($mails as $mail) {
            array_push($this->additionalMailReceivers, $mail);
        }
    }

    /**
     * @throws \Exception
     */
    public function getAccessToken(){
        $callService = new CallService(self::TOKEN_URL);
        $response    = $callService->send([
            'mail'      => urlencode($this->userMail),
            'pass'      => urlencode($this->userPass)
        ]);
        $data = json_decode($response);
        if (isset($data->error)) {
            throw new \Exception($data->error);
        }
        $this->userToken = $data->token;
    }

    /**
     * @return array $data
     * @throws \Exception
     */
    public function check(){
        $callService = new CallService(self::CHECK_URL);
        $response    = $callService->send([
            'composer_lock' => urlencode(file_get_contents($this->composerLockFile)),
            'composer_json' => urlencode(file_get_contents($this->composerJsonFile)),
            'project'       => urlencode($this->projectName),
            'token'         => urlencode($this->userToken),
            'add_mail'      => urlencode(json_encode($this->additionalMailReceivers))
        ]);
        $data = json_decode($response);
        if (isset($data->error)) {
            throw new \Exception($data->error);
        }
        return $data;
    }

    /**
     * @param string $composerJson path to file
     * @param string $composerLock path to file
     * @return mixed object
     */
    public function checkComposerFiles($composerJson, $composerLock){
        $this->setComposerJsonFile($composerJson);
        $this->setComposerLockFile($composerLock);
        return $this->check();
    }

    /**
     * @param string $composerJsonFile
     */
    public function setComposerJsonFile($composerJsonFile)
    {
        $this->composerJsonFile = $composerJsonFile;
    }

    /**
     * @param string $composerLockFile
     */
    public function setComposerLockFile($composerLockFile)
    {
        $this->composerLockFile = $composerLockFile;
    }

    /**
     * @param string $userMail
     */
    public function setUserMail($userMail)
    {
        $this->userMail = $userMail;
    }

    /**
     * @param string $userPass
     */
    public function setUserPass($userPass)
    {
        $this->userPass = $userPass;
    }

    /**
     * @param string $projectName
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }


}