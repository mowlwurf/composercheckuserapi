ComposerCheckUserApi
========================

ComposerCheckUserApi is a service to create call requests to ComposerCheckApi, which checks for outdated packages.
With this API your able to scale and handle the version checks of your composer packages completely by yourself. 

Usage
=====

```
use DevGarden\ComposerCheckUserApiBundle\Service\UserApiService;

// optional new UserApiService(), so you need to set this params with given setters
$service = new UserApiService('yourmail@domain.com','yourpassword');
/* optional use of setters
$service->setUserMail('example@domain.com');
$service->setUserPass('myRandomPassword');
*/
$service->setProjectName('myProject');

// adding additional receivers for control email
$service->addMailReceiver('yoursecondreceiver@example.com');
$service->addMailReceivers(['yoursecondreceiver@example.com','anotherreceiver@example.com']);

// catch exception for error handling
$service->getAccessToken();

$result = $service->checkComposerFiles('path/to/composer.json','path/to/composer.lock');

// or

$service->setComposerJsonFile('path/to/composer.json');
$service->setComposerLockFile('path/to/composer.lock');
$result = $service->check();

/*
 * array $result contains every package information including [Name, Version, Latest Stable, Latest]
 */
```

You are able to handle the response by yourself, additionally the result will be send in a rendered html table to your registered email address