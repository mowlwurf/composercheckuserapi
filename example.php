<?php

use DevGarden\UserApiBundle\Service\UserApiService;

// optional new UserApiService(), so you need to set this params with given setters
$service = new UserApiService('yourmail@domain.com','yourpassword');
/* optional use of setters
$service->setUserMail('example@domain.com');
$service->setUserPass('myRandomPassword');
*/
$service->setProjectName('myProject');

// adding additional receivers for control email
$service->addMailReceiver('yoursecondreceiver@example.com');
$service->addMailReceivers(['yoursecondreceiver@example.com','anotherreceiver@example.com']);

// catch exception for error handling
$service->getAccessToken();

$result = $service->checkComposerFiles('path/to/composer.json','path/to/composer.lock');

// or

$service->setComposerJsonFile('path/to/composer.json');
$service->setComposerLockFile('path/to/composer.lock');
$result = $service->check();

/*
 * array $result contains every package information including [Name, Version, Latest Stable, Latest]
 */